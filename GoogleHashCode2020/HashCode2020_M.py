'#ofbook|libs| days'
'score of the books(in order)'
'lib0'
'#of books|singup days|scan number'


# Input 
# BS [] is Book Score
# BN is Book Number
# LN is Library Number
# DN is Days Number
# LBN [] is Library Book Number
# LSD [] is Library Signup Process Days
# LSB [] is Libaray Ship Book Number
# LB [][] is Library books id


# Books Score = BestBooks * Floor (DaysLeft/LSB)
# Sort by book score -> high score


import math
import numpy as np

#Input
BS = []
LBN = []
LSD= []
LSB = []
LB = []
BN = 0
LN = 0
DN = 0      #Day Number
OutLN = 0
BMaxScore =0
Bid = []    # Book id (0 - BN), will be sorted by Book score
BF = []     #Book Flag (already sent book)
LF = []     #Library Flag (already used Library)
LS = []     #Library Score
LSortedBook = []    # [][] Library Sorted Book

#Output
OutLid = [] #Output of library in order
OutBook = [] #[][] Library sorted Book

def OutputFile(lines):
    
    f= open('b_output.txt','w')
    f.write(lines)
    f.close()


def ReadFile():
    global BN
    global LN
    global DN
    global BS
    global LB
    global LBN
    global LSD
    global LSB
    global LF
    global LS
    global BF
    global BMaxScore
    def getLine(line):
        return list(map(int,line.split(" ")))
    f = open('c_incunabula.txt','r')
    # first line
    BN,LN,DN = getLine(f.readline()) #6 2 
    # second line
    BS = getLine(f.readline())
    LB = [[] for i in range(LN)] 
    LBN = [0]*LN
    LSD = [0]*LN
    LSB = [0]*LN
    for i in range(LN):
        # third line
        LBN[i],LSD[i],LSB[i] = getLine(f.readline())
        #forth line
        LB[i] = getLine(f.readline())
    f.close()

    LF = [False for i in range(LN)]
    BF = [False for i in range(BN)]
    LS = [0 for i in range(LN)]
    BMaxScore= max(BS)
    

def CalculateLibraryScore(Lid):
    # Given DN, Library ID, 
    # score = floor((total - LSD[i])/LSB[i])*sum([])
    # OPTIMIZE : if before deadline contains all book, NO NEED SORT
    # OPTIMIZE : Flagged book dont sort it BF
    #left = score of the book | right = book number
    global BN
    global LN
    global DN
    global BS
    global LB
    global LBN
    global LSD
    global LSB
    global LSortedBook
    global BF
    
    bookCanScan = math.floor((DN-LSD[Lid])/LSB[Lid])
    
    LibraryBookId = [i for i in LB[Lid]]
    LibaryBookScore = [BS[book] for book in LB[Lid] if BF[book]==False]#Do FLAG here
    
    if(len(LibaryBookScore)>=bookCanScan):
        LibarySortedBook = LibraryBookId
    else:
        LibarySortedBook = [x for _,x in sorted(zip(LibaryBookScore,LibraryBookId),reverse=True)]
    LSortedBook.append(LibarySortedBook)
    
    score = sum(LibarySortedBook[:bookCanScan+1])/LSD[Lid]
    return score

def CalculateAllLibraryScore(): 
    global LN
    global LF
    global LS
    
    for i in range(LN):
        if(LF[i]==False):
            LS[i]=CalculateLibraryScore(i)

            
def FindMaxRateLibrary(libraryList):
    global LF
    targetLid=-1
    maxScore=-1000
    for i in range(len(libraryList)):
        if not LF[i]:
            if(libraryList[i]>maxScore):
                targetLid=i
                maxScore=libraryList[i]
    return targetLid

def ChooseLibrary():
    global LSortedBook
    global LS
    global DN
    
    #Find max score 
    Lid = FindMaxRateLibrary(LS)
    if(Lid==-1):
        return False
    #Final decide rate of library
    # print("Found Library",Lid,LSortedBook)
    SignUpLibrary(Lid,LSortedBook[Lid])

    DN=DN-LSD[Lid]

    return True if DN>1 else False
    

def SignUpLibrary(Lid,BookOrder):
    global LF
    global OutBook
    global BF
    global OutLN
    #Library Id, print what library choose and book order
    LF[Lid]=True
    
    OutLN=OutLN+1
    OutLid.append(Lid)
    OutBook.append(BookOrder)
    for i in range(len(BookOrder)):
        BF[BookOrder[i]]=True
    
    # f = open('d_output.txt','a')
    # f.write("{} {}\n".format(Lid,len(BookOrder)))
    # f.write("{}\n".format(" ".join(list(map(str,BookOrder)))))
    # f.close()


def WriteAnswer():
    global OutLid
    global OutBook
    global OutLN
    # with open("test.txt") as f:
    #     lines = f.readlines()
    #     #number of lib
    #     lines[0] = OutLN
    #     # #list of li
    #     # lines[1] 
        
        
    f = open('c_output.txt','w')
    f.write(str(len(OutLid))+"\n")
    
    for i in range(len(OutLid)):
        # print(OutBook[i])
        f.write("{} {}\n".format(OutLid[i],len(OutBook[i])))
        f.write("{}\n".format(" ".join(list(map(str,OutBook[i])))))
    f.close()

    pass
def CalculateTotalScore():
    global OutLid
    global OutBook
    outBookAll = []
    for i in range(len(OutLid)):
        outBookAll = []
        totalScore = 0
        for book in OutBook[i]:
            outBookAll.append(book)
            totalScore +=BS[book]
    return totalScore

def main():
    global LB
    global Bid
    #functions to operate
    ReadFile()
    Bid=[i for i in range(BN)]
    
    # f = open('b_read_on.txt','a')
    # f.write("123\n")
    # f.close()



    i=0
    CalculateAllLibraryScore()
    while(ChooseLibrary()==True):
        i=i+1
        print("Iteration",i)
        # LSortedBook.clear()
        CalculateAllLibraryScore()
    
    WriteAnswer()


if __name__ == "__main__":
     main()







    


    
    
    