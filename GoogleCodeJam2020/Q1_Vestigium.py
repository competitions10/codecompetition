
T=int(input())
for t in range(T):
	N=int(input())
	matrix=[[0 for i in range(N)] for i in range(N)]
	rowd=0
	cold=0
	trace=0
	for i in range(N):
		valueInput=list(map(int,input().strip().split()))[:N]
		rowFlag=[False for i in range(N+1)]
		rowDupFlag=False
		for j in range(N):
			v=valueInput[j]
			matrix[j][i]=v
			if rowFlag[v]==True:
				rowDupFlag=True
			rowFlag[v]=True
		if rowDupFlag==True:
			rowd+=1
	for i in range(N):
		colFlag=[False for i in range(N+1)]
		colDupFlag=False
		for j in range(N):
			if colFlag[matrix[i][j]]==True:
				colDupFlag=True
			colFlag[matrix[i][j]]=True
		if colDupFlag==True:
			cold+=1
	for i in range(N):
		trace+=matrix[i][i]
	print("Case #"+str(t+1)+":",trace,rowd,cold)
