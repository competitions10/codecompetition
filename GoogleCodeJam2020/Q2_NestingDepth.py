T=int(input())

for t in range(T):
	s=input()
	ans=""
	for i in range(len(s)+1):
		indexA=i-1
		indexB=i
		a=0
		b=0

		if indexA==-1:
			a=0
			b=s[indexB]
		elif indexB>=len(s):
			a=s[indexA]
			b=0
		else:
			a=s[indexA]
			b=s[indexB]

		symbol=")"
		if(int(b)-int(a)>0):
			symbol="("
		for j in range(abs(int(b)-int(a))):
			ans=ans+symbol
		if(i<len(s)):
			ans=ans+str(b)
	print("Case #"+str(t+1)+": "+ans)