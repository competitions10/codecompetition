T=int(input())

for t in range(T):
	N=int(input())
	tasks=[]
	assign=["N" for i in range(N)]
	for n in range(N):
		s,e=map(int,input().split())
		tasks.append((s,e))
	# Greedy asign C first
	currentHead=0
	while(True):
		minTail=9999
		minIndex=0
		for i in range(N):
			if tasks[i][0]>=currentHead:
				if tasks[i][1]<minTail:
					minTail=tasks[i][1]
					minIndex=i

		if(minTail==9999):
			break
		assign[minIndex]="C"
		currentHead=tasks[minIndex][1]
		tasks[minIndex]=(99999,99999)

		
	# Greedy asign J first
	currentHead=0
	while(True):
		minTail=9999
		minIndex=0
		for i in range(N):
			if tasks[i][0]>=currentHead:
				if tasks[i][1]<minTail:
					minTail=tasks[i][1]
					minIndex=i

		if(minTail==9999):
			break
		assign[minIndex]="J"
		currentHead=tasks[minIndex][1]
		tasks[minIndex]=(99999,99999)

	
	ans=""
	for i in range(N):
		if assign[i]=="N":
			print("Case #"+str(t+1)+": "+"IMPOSSIBLE");
			ans="IMPOSSIBLE"
			break
		ans+=assign[i]
	if(ans!="IMPOSSIBLE"):
		print("Case #"+str(t+1)+": "+ans);






