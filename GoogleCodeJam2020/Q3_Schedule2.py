


T=int(input())

for t in range(T):
	N=int(input())
	tasks=[]
	assign=["N" for i in range(N)]
	graph=[[] for i in range(N)]
	
	fail=False
	for n in range(N):
		s,e=map(int,input().split())
		tasks.append((s,e))

	for i in range(N):
		for j in range(N):
			if i!=j:
				if(tasks[i][1]>tasks[j][0] and tasks[i][0]<tasks[j][1]):
					graph[i].append(j)

	def flip(p):
		if(p=="C"):
			return "J"
		else:
			return "C"

	def dfs(i,p):
		global fail
		if(fail==True):
			return
		if(assign[i]!="N" and assign[i]!=p):
			fail=True
		elif(assign[i]=="N"):
			assign[i]=p
			for j in graph[i]:
				if(assign[j]!="N" and assign[j]!=flip(p)):
					fail=True
				elif(assign[j]=="N"):
					dfs(j,flip(p))
		
			
	for i in range(N):
		if(assign[i]=="N"):
			result=dfs(i,"C")

	if(fail==True):
		print("Case #"+str(t+1)+": "+"IMPOSSIBLE");
	else:
		ans=""
		for i in range(N):
			if assign[i]=="N":
				ans+="C"
				break
			ans+=assign[i]
		if(ans!="IMPOSSIBLE"):
			print("Case #"+str(t+1)+": "+ans);
