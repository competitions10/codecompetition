
T=int(input())

for t in range(T):
	x,y,s=input().split()
	x=int(x)
	y=int(y)

	
	count=0
	chaseX=0
	chaseY=0
	breaking=0
	left=len(s)
	for d in s:
		left-=1
		if(d=='S'):
			if(y>0):
				y-=1
				count+=1
			elif(y<=0):
				if(chaseY>0):
					count+=2
				chaseY-=1
			
		elif(d=='N'):
			if(y<0):
				y+=1
				count+=1
			elif(y>=0):
				if(chaseY<0):
					count+=2
				chaseY+=1
		elif(d=='E'):
			if(x<0):
				x+=1
				count+=1
			elif(x>=0):
				if(chaseX<0):
					count+=2
				chaseX+=1
				
		elif(d=='W'):
			if(x>0):
				x-=1
				count+=1
			elif(x<=0):
				if(chaseX>0):
					count+=2
				chaseX-=1

		# print("x",x,"y",y,"count",count,"chaseXY",abs(chaseX),abs(chaseY),"x+y",x+y,"left",left)
		if(count>=x+y):
			break

	if(count>=x+y):
		print("Case #"+str(t+1)+": "+str(chaseX+chaseY+count))
	else:
		print("Case #"+str(t+1)+": IMPOSSIBLE")
		

# elif(d=='E'):
# 			if(x<0):
# 				x+=1
# 				count+=1
# 			elif(x>=0):
# 				if(chaseX>0):
# 					count+=2
# 				chaseX-=1
# 		elif(d=='W'):
# 			if(x>0):
# 				x-=1
# 				count+=1
# 			elif(x<=0):
# 				if(chaseX<0):
# 					count+=2
# 				chaseX+=1