from data import read_data,output_data,filenames
from algo import weighted_road


def helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch = {}

    sch={}
    for i in range(num_intersects):
        sch[i]=[]
        for s in intersect2streets[i]:
            sch[i].append((s.name,1))
    return sch



if __name__ == "__main__":
    for key, val in filenames.items():
        duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id = read_data(fname=f"inputs\{filenames[key]}")

        # sch = helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)
        sch = weighted_road(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)
        
        output_data(f"results_marco\{filenames[key]}",sch)

#keychron