from data import read_data,output_data,filenames


def helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    return {}

def merge_sch(sch1,sch2,weight):
    def linear_wegight(d1,d2,weight):
        return int(d1 * weight + d2 * (1-weight))
    sch={}
    # weight 1 => sch1 , weight 0 => shc2 -> sch1*(weight)+sch2*(1-weight)
    #{[(name,duration),(name,duration)],[(name,duration)]}
    # merge every traffic light
    sch3 = [0] * len(sch1)
    for i in range(len(sch1)):
        name1, duration1 = sch1[i]
        name2, duration2 = sch2[i]
        sch3[i] = (linear_wegight(duration1,duration2,weight[i]))
    return sch3


def read_data(fname):
    with open(fname, mode='r') as fp:
        lines = [ line.split('\n')[0] for line in fp.readlines()]
        duration, num_intersects, num_streets, num_cars, bonus_map = [int(val) for val in lines[0].split()]
        intersect2streets = { i: [] for i in range(num_intersects)} 
        streets = []
        streetname2id = {}

        street_id = 0
        for i in range(1, num_streets+1):
            data = lines[i].split()
            streets.append(Street(
                int(data[0]),
                int(data[1]),
                data[2],
                int(data[3]),
            ))
            intersect2streets[int(data[1])].append(streets[-1])
            streetname2id[data[2]] = street_id 
            street_id += 1
            
        car_paths = []
        for j in range(i+1, len(lines)):
            data = lines[j].split()
            path = []
            for k in range(1, len(data)):
                path.append(data[k]) 
            car_paths.append(path)
    # 6 4 5 2 1000 (2,0,street-name,1)           
    return duration, num_intersects, num_streets, num_cars, bonus_map, streets, car_paths, intersect2streets, streetname2id

if __name__ == "__main__":
    for key, val in filenames.items():
        duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id = read_data(fname=f"inputs\{filenames[key]}")
        result = helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)

        sch = helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)   

        output_data(f"results_richard\{filenames[key]}",sch)

'''
1. (Edward) SimpleCount - Count the number of cars passing each outgoing street --> weight
2. WeightedRoad - Assign weight to each according to length road
3. TimedCount - Simulation Count the number of cars by each time frame
4. Starting point selection - Low Priority 
5. Score function
'''