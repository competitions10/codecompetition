import numpy as np
import collections
def simple_Algo(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch={}
    for i in range(num_intersects):
        sch[i]=[]
        for s in intersect2streets[i]:
            sch[i].append((s.name,1))
    return sch



def simple_count(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch={}
    street_cnts = [ 0 for i in range(num_streets)]
    for path in paths:
        for street_name in path:
            street_cnts[streetname2id[street_name]] += 1
    
    for i in range(num_intersects):
        time_list = np.array([street_cnts[streetname2id[street.name]] for street in intersect2streets[i]])
        if np.min(time_list) == 0:
            continue
        else:
            time_list = np.round(time_list / np.maximum(np.min(time_list), 1))
            time_list = time_list.tolist()
            time_list = [ (street.name, int(time_list[idx])) for idx, street in enumerate(intersect2streets[i]) if int(time_list[idx]) > 0 ]

        sch[i] = time_list
    
    return sch

def simple_count_weighted(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch={}
    street_cnts = [ 0 for i in range(num_streets)]
    lengths = [ [streets[streetname2id[street_name]].duration for street_name in path] for path in paths ]
    sumed_length = [ sum(length) for length in lengths]
    # print(sumed_length)
    for idx, path in enumerate(paths):
        for street_name in path:
            street_cnts[streetname2id[street_name]] += 1 / sum(lengths[idx])
    
    # print(street_cnts)
    for i in range(num_intersects):
        time_list = np.array([street_cnts[streetname2id[street.name]] for street in intersect2streets[i] if street_cnts[streetname2id[street.name]] > 0])
        streets_filtered =  [ street for street in intersect2streets[i] if street_cnts[streetname2id[street.name]] > 0]
        if len(time_list) == 0:
            continue
        else:
            time_list = time_list / np.sum(time_list)
            time_list = np.round(time_list / np.min(time_list))
            time_list = time_list.tolist()
            time_list = [ (streets_filtered[idx].name, int(np.clip(time_list[idx], 1, 20))) for idx, stret in enumerate(streets_filtered) ]

        sch[i] = time_list
    
    return sch



def weighted_road(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch = {}
    carDuration=[]
    
    for i in range(num_cars):
        carDuration.append(0)
        for p in paths[i]:
            carDuration[-1]+=streets[streetname2id[p]].duration
    
    normalizedMax=10
    maxCarDuration=max(carDuration)
    carDuration=[ (i/maxCarDuration)*normalizedMax for i in carDuration]
    carScore=[ normalizedMax-i+1 for i in carDuration]
    

    streetScore=[[] for i in range(num_streets)]
    for i in range(num_cars):
        for p in paths[i]:
            streetScore[streetname2id[p]].append(carScore[i])

    streetAvg=[ 0 if len(score)==0 else sum(score)/len(score) for score in streetScore]


    for i in range(num_intersects):

        temp=[]
        for s in intersect2streets[i]:
            value=int(round(streetAvg[streetname2id[s.name]]))#/len(intersect2streets[i])))
            if(value!=0):
                temp.append((s.name,value))
        if(len(temp)>0):
            sch[i]=temp
    
    

    return sch
        
    

def timed_count(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch={}

def merge_preprocessing(dic):
    def sort_dict_by_key(dic):
        return dict(collections.OrderedDict(sorted(dic.items())))
    def sort_list_of_tuple(data):
        return sorted(data, key=lambda tup: tup[0])
    dic = sort_dict_by_key(dic)
    for key,list_of_tuple in dic.items():
        dic[key] = sort_list_of_tuple(list_of_tuple)
    return dic

def merge_sch(sch1, sch2, weight):
    def linear_wegiht(d1, d2, weight):
        return int(d1 * weight + d2 * (1 - weight))
    def fill_empty_list(lis, n, fillvalue=(None,None)):
        return lis + [fillvalue] * (len(n) - len(lis))
    def cal_weighting(tup1, tup2,weight):
        # list of tup1 is shorter than list of tup 2
        if tup1[0] is None:
            return tup2
        elif tup2[0] is None:
            return tup1
        else:
            name1, duration1 = tup1
            name2, duration2 = tup2
            return name1, linear_wegiht(duration1, duration2, weight)
    def fill_empty_key(shorter_dic, longer_dic):
        for key in longer_dic.keys():
            if key not in shorter_dic.keys():
                shorter_dic[key] = []
        return shorter_dic
    # weight 1 => sch1 , weight 0 => shc2 -> sch1*(weight)+sch2*(1-weight)
    # sch1 = {0:[("a",1),("b",2)],1:[("c",3)]}
    # sch2 = {0:[("a",3),("b",4)],1:[("c",6)]}
    # weight = 0.6
    # merge every traffic light

    
    if len(sch1.keys()) < len(sch2.keys()):
        shorter_dic = sch1
        longer_dic = sch2
        sch1 = fill_empty_key(shorter_dic,longer_dic)
    else:
        shorter_dic = sch2
        longer_dic = sch1
        sch2 = fill_empty_key(shorter_dic,longer_dic)
    sch1 = merge_preprocessing(sch1)
    sch2 = merge_preprocessing(sch2)
    sch = {}
    for key,name_duration_list in sch1.items():
        len1 = len(sch1[key])
        len2 = len(sch2[key])
        weight_list_len = max(len1, len2)
        if len1 < len2:
            shoter_list = sch1[key]
            longer_list = sch2[key]
            sch1[key] = fill_empty_list(shoter_list, longer_list)
        else:
            shoter_list = sch2[key]
            longer_list = sch1[key]
            sch2[key] = fill_empty_list(shoter_list, longer_list)

        weighted_list = [(0,0)] * weight_list_len

        for i in range(weight_list_len):
            tup = cal_weighting(sch1[key][i],sch2[key][i],weight)
            weighted_list[i] = tup
    # merge every traffic light
    sch1 = merge_preprocessing(sch1)
    sch2 = merge_preprocessing(sch2)
    sch = {}
    for key,name_duration_list in sch1.items():
        len1 = len(sch1[key])
        len2 = len(sch2[key])
        weight_list_len = max(len1, len2)
        if len1 < len2:
            shoter_list = sch1[key]
            longer_list = sch2[key]
            sch1[key] = fill_empty_list(shoter_list, longer_list)
        else:
            shoter_list = sch2[key]
            longer_list = sch1[key]
            sch2[key] = fill_empty_list(shoter_list, longer_list)

        weighted_list = [(0,0)] * weight_list_len

        for i in range(weight_list_len):
            tup = cal_weighting(sch1[key][i],sch2[key][i],weight)
            weighted_list[i] = tup
        sch[key] = weighted_list
    return sch


def pop_zero_duration(intersection_streets):
    #[("a",0),("b",2)]
    return [ (street_name,duration)  for street_name, duration in intersection_streets if duration!=0]

    
def timed_count(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    print()
    pass

#### 
## TODO:
'''

1. (Edward) SimpleCount - Count the number of cars passing each outgoing street --> weight
2. WeightedRoad - Assign weight to each according to length road
3. TimedCount - Simulation Count the number of cars by each time frame
4. Starting point selection - Low Priority 
5. (Seems cant)Score function
'''
