from data import read_data,output_data,filenames
from algo import simple_Algo


def helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    pass

if __name__ == "__main__":
    for key, val in filenames.items():
        duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id = read_data(fname=f"inputs\{filenames[key]}")
        result = helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)

        sch = helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)   

        output_data(f"results\alpha{filenames[key]}",sch)
