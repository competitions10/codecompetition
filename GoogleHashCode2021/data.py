from collections import namedtuple

Street = namedtuple( 'Street',
[
'start_point',
'end_point',
'name',
'duration',
]
)

filenames = { 
    1: 'a.txt',
    2: 'b.txt',
    3: 'c.txt',
    4: 'd.txt',
    5: 'e.txt',
    6: 'f.txt',
}

def read_data(fname):
    with open(fname, mode='r') as fp:
        lines = [ line.split('\n')[0] for line in fp.readlines()]
        duration, num_intersects, num_streets, num_cars, bonus_map = [int(val) for val in lines[0].split()]
        intersect2streets = { i: [] for i in range(num_intersects)} 
        streets = []
        streetname2id = {}

        street_id = 0
        for i in range(1, num_streets+1):
            data = lines[i].split()
            streets.append(Street(
                int(data[0]),
                int(data[1]),
                data[2],
                int(data[3]),
            ))
            intersect2streets[int(data[1])].append(streets[-1])
            streetname2id[data[2]] = street_id 
            street_id += 1
            
        car_paths = []
        for j in range(i+1, len(lines)):
            data = lines[j].split()
            path = []
            for k in range(1, len(data)):
                path.append(data[k]) 
            car_paths.append(path)
        

        
        
        
    return duration, num_intersects, num_streets, num_cars, bonus_map, streets, car_paths, intersect2streets, streetname2id

def output_data(fname,sch):
    
    # print(f"score:{score_func(sch)}")

    with open(fname,mode='w') as fp:
        fp.write(f'{len(sch)}\n')
        for i, intersection_streets in sch.items():
            fp.write(f"{i}\n")
            fp.write(f"{len(intersection_streets)}\n")
            for street_name, duration in intersection_streets:
                fp.write(f"{street_name} {duration}\n")
        fp.close()

# [[(street_name, time) ...] ...]
if __name__ == "__main__":
    for key, val in filenames.items():
        duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id = read_data(fname=filenames[key])
        print(f'duration: {duration}, num_intersects: {num_intersects}, num_streets: {num_streets}, num_cars: {num_cars}, bonus_map: {bonus_map}')
        # for street in streets:
        #     print(f'street: {street}')
        # for path in paths:
        #     print(f'path: {path}')
        # break


