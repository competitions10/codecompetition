from data import read_data,output_data,filenames
from algo import weighted_road,simple_count,merge_sch


def helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch = {}

if __name__ == "__main__":
    for key, val in filenames.items():
        duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id = read_data(fname=f"inputs\{filenames[key]}")

        sch1 = weighted_road(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)
        sch2 = simple_count(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)
        
        sch=merge_sch(sch1,sch2,0.5)
        output_data(f"results\{filenames[key]}",sch)

#keychron7072
