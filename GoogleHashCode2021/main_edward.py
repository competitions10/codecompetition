from data import read_data,output_data,filenames
from algo import simple_count, simple_count_weighted


def helper(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id):
    sch = {}

if __name__ == "__main__":
    for key, val in filenames.items():
        duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id = read_data(fname=f"inputs\{filenames[key]}")

        # print(paths)
        sch = simple_count_weighted(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)
        # print(sch)
        # break
        # sch = simple_Algo(duration, num_intersects, num_streets, num_cars, bonus_map, streets, paths, intersect2streets, streetname2id)   
        # break
        output_data(f"results_edward\{filenames[key]}",sch)

#keychron