
import math


fileInput="cheeseburger_corollary_2_input.txt"
fileOutput="A2Out.txt"


def GetBurger(A,B,C):
    print(A,B,C)
    perBurgerA = A
    perBurgerB = B/2
        
    if(perBurgerA<perBurgerB):
        return math.floor(C/A)
    else:
        return max(0,max(math.floor(C/B)*2-1,math.floor((C-A)/B)*2+1) )

T=0
with open(fileInput) as file, open(fileOutput,"w") as output:
    T=int(file.readline())
    for i in range(T):
        A,B,C = map(float, file.readline().split())
        

        ans = f"Case #{i+1}: {GetBurger(A,B,C)}\n"
        output.write(ans)

