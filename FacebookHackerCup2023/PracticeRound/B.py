
fileInput="dim_sum_delivery_input.txt"
fileOutput="BOut.txt"


T=0
with open(fileInput) as file, open(fileOutput,"w") as output:
    T=int(file.readline())
    for i in range(T):
        R,C,A,B = map(int, file.readline().split())

        valid = R>C
        ans = f"Case #{i+1}: {'YES' if valid else 'NO'}\n"
        output.write(ans)

