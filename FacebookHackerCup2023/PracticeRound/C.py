
import math


fileInput="two_apples_a_day_input.txt"
fileOutput="COut.txt"
def GetMatchByKey(apples,key):
    #This is not matches and find the missing one by key
    ansList = []
    for i in range(math.floor((len(apples)+1)/2)):
        if(apples[i]+apples[-i-1]!=key):
            break

    replaceMinTarget = i
    tempApple = list(apples)
    ans = key-apples[-i-1]
    tempApple.insert(replaceMinTarget,key-apples[-i-1])
    if(ans>0 and IsMatchByKey(tempApple,key)):
        ansList.append(ans)
    
    replaceMaxTarget = len(apples)-i-1+1
    tempApple = list(apples)
    ans = key-apples[i]
    tempApple.insert(replaceMaxTarget,key-apples[i])
    if(ans>0 and IsMatchByKey(tempApple,key)):
        ansList.append(ans)
    
    return ansList


def IsMatchByKey(apples,key):
    for i in range(math.floor(len(apples)/2)):
        if(apples[i]+apples[-i-1]!=key):
            return False
    return True

def GetApple(days,apples):
    # There are 3 cases, replace Min, replace Max, replace middle ones
    if(days==1):
        return 1
    
    apples.sort()
    ansList = []
    # replace Max
    tempApple = list(apples)
    key = apples[-1] + apples[1]
    ans = key - apples[0]
    tempApple.append(ans)
    isMatch = IsMatchByKey(tempApple,key)
    if(isMatch and ans>0):
        ansList.append(ans)

    # replace Min
    tempApple = list(apples)
    key = apples[-2] + apples[0]
    ans = key - apples[-1]
    tempApple.insert(0,ans )    
    isMatch = IsMatchByKey(tempApple,key)
    if(isMatch and ans>0):
        ansList.append(ans)

    # set max as max
    tempApple = list(apples)
    key = apples[-1] + apples[0]    
    ans = GetMatchByKey(tempApple,key)
    
    for a in ans:
        ansList.append(a)
    print(ansList)


    valid = True
    return min(ansList) if len(ansList)>0 else -1

T=0
with open(fileInput) as file, open(fileOutput,"w") as output:
    T=int(file.readline())
    for i in range(T):
        days = int(file.readline())
        apples = list(map(int, file.readline().split()))
        
        ans = f"Case #{i+1}: {GetApple(days,apples)}\n"
        output.write(ans)

