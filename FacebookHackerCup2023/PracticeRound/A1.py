
fileInput="cheeseburger_corollary_1_input.txt"
fileOutput="A1Out.txt"


T=0
with open(fileInput) as file, open(fileOutput,"w") as output:
    T=int(file.readline())
    for i in range(T):
        S,D,K = map(int, file.readline().split())
        valid = (S+2*D >= K) and (2*(S+D) >= K+1)
        ans = f"Case #{i+1}: {'YES' if valid else 'NO'}\n"
        output.write(ans)

