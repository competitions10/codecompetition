import functools
from collections import defaultdict 
import sys
fileInput = "ready_go_part_2_input.txt"
fileOutput = "A2Output2.txt"

Empty = '.'
White = 'W'
Black = 'B'

flag = {}
targetPair = (-1,-1)

# sys.setrecursionlimit(3000*3000)

# def FindCapture(x,y,board):
    
#     # Do a BFS flood fill if any white is nearby
#     if(board[y][x]!=Empty):
#         return 0
#     result = DFS(x,y,board,True)
#     # print(x,y,"=",result)
#     return result

def DFS(x,y,board):
    global targetPair
    if((x,y) in flag):
        return 0
    flag[(x,y)] = True
    
    R = len(board)
    C = len(board[0])
    
    # print(f' flag {(x,y)}')
    result = 0

    nextPos = [(x+1,y),(x-1,y),(x,y+1),(x,y-1)]
    for nx,ny in nextPos:
        if(nx>=0 and nx<C and ny>=0 and ny<R):
            if((nx,ny) in flag):
                continue
            # print(f'  - Check Y {ny} X {nx}')
            if(board[ny][nx]==White):
                newResult = DFS(nx,ny,board)
                # print(f' DFS newResult {newResult}')
                if(newResult == -1 or result==-1):
                    result = -1
                else:
                    result += newResult + 1
                
            elif(board[ny][nx]==Empty): 
                
                if(targetPair != (-1,-1) ):
                    if(targetPair != (nx,ny)):
                        # print(f' B2 targets {targetPair} result {x},{y} -> {nx},{ny}')
                        result = -1
                        
                else:
                    # print(f' B2 Set Target {targetPair} result {x},{y} -> {nx},{ny}')
                    targetPair = (nx,ny)
                    
            
    
    # print(f' Result {(x,y)} = {result} {targetPair}')
    return result

def Solve(R,C,board):
    global flag
    global targetPair
    flag.clear()

    def defValue():
        return 0
    
    total = defaultdict(defValue)

    result = 0
    for r in range(R):
        for c in range(C):            
            if(board[r][c]==White):
                targetPair = (-1,-1)
                result = DFS(c,r,board)
                if(result>=0 and targetPair!= (-1,-1)):
                    total[targetPair] += result +1
                    # print(f' {result} {targetPair}')

    if(len(total)==0):
        return 0
    return total[max(total, key=total.get)]

T=0
with open(fileInput) as file, open(fileOutput,"w") as outputFile:
    ans=""
    T=int(file.readline())
    for i in range(T):
        R,C = list(map(int, file.readline().split(" ")))
        board = []
        for r in range(R):
            board.append(file.readline()[:-1])

        # for b in board:
        #     print(b)
        
        ans += f"Case #{i+1}: {Solve(R,C,board)}\n"
        print("Hello"+ans)
        outputFile.write(ans)

