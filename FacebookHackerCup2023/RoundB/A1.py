import functools

fileInput = "ready_go_part_1_input.txt"
fileOutput = "A1Output.txt"

Empty = '.'
White = 'W'
Black = 'B'

flag = {}

def DFS(x,y,board, isFirstLayer):
    if((x,y) in flag):
        return False if isFirstLayer else True
    flag[(x,y)] = True
    R = len(board)
    C = len(board[0])
    
    # print(f' flag {(x,y)}')
    result = False if isFirstLayer else True

    nextPos = [(x+1,y),(x-1,y),(x,y+1),(x,y-1)]
    for nx,ny in nextPos:
        if(nx>=0 and nx<C and ny>=0 and ny<R):
            if((nx,ny) in flag):
                continue
            
            # print(f'  - Check Y {ny} X {nx}')
            if(board[ny][nx]==White):
                if(isFirstLayer):
                    flag.clear()
                    flag[(x,y)] = True
                    result |= DFS(nx,ny,board,False)
                else:
                    # print(f' Proceed -> {nx} {ny}')
                    newResult = DFS(nx,ny,board,False)
                    result &= newResult
                
            elif(board[ny][nx]==Empty):
                if(isFirstLayer):
                    result |= False
                else:
                    result = False
                    break
    
    
    # print(f' flag {(x,y)} = {result}')
    return result

def FindCapture(x,y,board):
    global flag
    flag.clear()
    # Do a BFS flood fill if any white is nearby
    if(board[y][x]!=Empty):
        return False
    result = DFS(x,y,board,True)
    # print(x,y,"=",result)
    return result

def Solve(R,C,board):
    
    for r in range(R):
        for c in range(C):
            if(FindCapture(c,r,board)):
                return "YES"
    return "NO"

T=0
with open(fileInput) as file, open(fileOutput,"w") as output:

    T=int(file.readline())
    for i in range(T):
        R,C = list(map(int, file.readline().split(" ")))
        board = []
        for r in range(R):
            board.append(file.readline()[:-1])

        # for b in board:
        #     print(b)
        
        ans = f"Case #{i+1}: {Solve(R,C,board)}\n"
        print(ans)
        output.write(ans)

