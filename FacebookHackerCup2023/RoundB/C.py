from collections import defaultdict
import time
import queue

# Jump the number of cases where count are impossible

with open("wiki_race_input.txt","r") as finput, open("COut.txt","w") as foutput: 
    T = int(finput.readline()) 
    fullStart = time.time()
    for t in range(T): 

        start = time.time()
        # Read whole graph 
        N = int(finput.readline()) 
        parent = [-1] + list(map(lambda p:int(p)-1,finput.readline().split(' '))) 
        leaves = set(range(N)).difference(set(parent)) 
        contents=[] 
        allTopic = set() 
        def defValue():
            return 0
        topicCount = defaultdict(defValue)
        for n in range(N): 
            content = finput.readline()[:-1].split(' ') 
            M = int(content[0]) 
            content = set(content[1:]) 
            # allTopic = allTopic.union(content) 
            for c in content:
                if(c not in allTopic):
                    allTopic.add(c)
                topicCount[c]+=1
            contents.append(content) 
        
        # construct the child list for nodes 
        child = [[] for i in range(N)] 
        childDict = [{} for i in range(N)]
        for i,p in enumerate(parent): 
            if(p!=-1): 
                childDict[p][i] = len(child[p])
                child[p].append(i) 
        
        # print(f"case {t+1}")
        # end = time.time()
        # print(end-start)
        # continue

        # merge all 1-parent 1-child node 
        root = 0 
        for i,c in enumerate(child): 
            if(len(c)==1): 
                if(i==root): 
                    root=c[0] 
                parent[c[0]] = parent[i] 
                contents[c[0]] = contents[c[0]].union(contents[i]) 
                contents[i].clear()

                if(parent[i]!=-1):
                    child[parent[i]][childDict[parent[i]][i]] = c[0]
                    childDict[parent[i]][c[0]] = childDict[parent[i]][i]
                    childDict[parent[i]].pop(i)

                child[i]=[] 
                parent[i] = -1 

        # print(f"case {t+1}")
        # end = time.time()
        # print(end-start)
        # continue


        # each time only find the parents 
        mutualTopicCount = 0
        for topic in allTopic:
            if(topicCount[topic]<len(leaves)):
                mutualTopicCount+=1
                continue
            nodeNeed = [False for i in range(N)]    # 0 is default, 1 is Contains, 2 is Need
            stack = [leaf for leaf in leaves]
            while(len(stack)>0): 
                currentNode = stack.pop()
                if(topic not in contents[currentNode]):
                    if(nodeNeed[parent[currentNode]]==True):
                        mutualTopicCount+=1
                        break
                    else:
                        if(parent[currentNode]==-1):
                            mutualTopicCount+=1
                            break
                        nodeNeed[parent[currentNode]] = True
                        stack.append(parent[currentNode])











        # Do DFS traverse on each topic from root 
        # mutualTopicCount=0
        # for topic in allTopic: 
        #     stack = [root] 
        #     childTraverse = [0 for i in range(N)]
        #     nodeNeed = [False for i in range(N)] 
        #     while(len(stack)>0): 
        #         currentNode = stack[-1]
        #         if (childTraverse[currentNode]==len(child[currentNode])):
        #             stack.pop()
        #             # print(currentNode, childTraverse[currentNode],"parent",parent[currentNode], nodeNeed[parent[currentNode]])
        #             if topic not in nodeContent[currentNode]:
        #                 if nodeNeed[parent[currentNode]]:
        #                     mutualTopicCount+=1
        #                     break

        #                 if(len(child[currentNode])==0):
        #                     nodeNeed[parent[currentNode]]=True
        #                 elif(nodeNeed[currentNode]):
        #                     if(parent[currentNode]==-1):
        #                         mutualTopicCount+=1
        #                         break
        #                     nodeNeed[parent[currentNode]]=True
                    
        #         else:
        #             stack.append(child[currentNode][childTraverse[currentNode]])
        #             childTraverse[currentNode]+=1
        #             print(f" {topic} {currentNode}: {stack} {childTraverse[currentNode]}  total {len(child[currentNode])}")
                
            # Check require topic or not 
            # stack is the parent node that have been called, store the child Index as well  
            # print(topic,nodeNeed)
        
    
        # print(allTopic) 
        # print(nodeContent) 
        # print(child) 
        # print(parent) 
        # print(mutualTopic)
        # print()

        ans = f'Case #{str(t+1)}: {str(len(allTopic)-mutualTopicCount)}\n'
        print(ans)
        foutput.write(ans)
        end = time.time()
        print(end-start)

    end = time.time()
    print(end-fullStart)