
fileInput="here_comes_santa_claus_input.txt"
fileOutput="AOut.txt"
infinity =  float("inf")

def FindDist(a1,a2,b1,b2):
    return (b1+b2)/2 - (a1+a2)/2 

def Solve(house):
    # house = sorted(house)
    house.sort()
    print(house)
    if((len(house)-4)!=1):
        return FindDist(house[0],house[1],house[-1],house[-2])
    else:
        return max(FindDist(house[0],house[2],house[-1],house[-2]),FindDist(house[0],house[1],house[-1],house[-3]))


T=0
with open(fileInput) as file, open(fileOutput,"w") as output:

    T=int(file.readline())
    for i in range(T):
        N = int(file.readline())
        house = list(map(int, file.readline().split()))

        
        
        ans = f"Case #{i+1}: {Solve(house)}\n"
        output.write(ans)

