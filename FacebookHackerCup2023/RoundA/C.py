
fileInput="back_in_black_chapter_1_sample_input.txt"
fileOutput="COut.txt"

# def FindFactors(i):
    # return list of factors and will be checked


def Solve(P):
    return 0



T=0
with open(fileInput) as file, open(fileOutput,"w") as output:

    T=int(file.readline())
    for i in range(T):
        P = int(file.readline())
        ans = f"Case #{i+1}: {Solve(P)}\n"
        print(ans)
        output.write(ans)

