
fileInput="sum_41_chapter_2_validation_input.txt"
fileOutput="B2Out.txt"
infinity =  float("inf")


def Solve(P):
    ans = []
    totalAns = 0

    if(P==1):
        while(totalAns<41):
            ans.append(1)
            totalAns+=1
        return f"{str(len(ans))} {' '.join(map(str,ans))}"
    
    i=2
    while(True):
        if(P%i==0):
            print(ans, P)
            if(totalAns+i>41):
                return "-1"
            totalAns+=i
            ans.append(i)
            P/=i
            if(P==1 and totalAns<=41):
                while(totalAns<41):
                    ans.append(1)
                    totalAns+=1

                return f"{str(len(ans))} {' '.join(map(str,ans))}"
        else:
            i+=1

    return "-1"
T=0
with open(fileInput) as file, open(fileOutput,"w") as output:

    T=int(file.readline())
    for i in range(T):
        P = int(file.readline())
        ans = f"Case #{i+1}: {Solve(P)}\n"
        print(ans)
        output.write(ans)

