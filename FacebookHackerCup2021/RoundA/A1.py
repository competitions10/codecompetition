inF=open("weak_typing_chapter_1_input.txt","r")
outF=open("A1_out.txt","w")
# 1. F is not important

T=int(inF.readline())
for t in range(T):
	N=int(inF.readline())
	S=inF.readline()[:-1]

	count=0
	S=S.replace("F","")

	if(len(S)>0):
		now=S[0]
		for s in S:
			if(s!=now):
				now=s
				count+=1
	else:
		count=0

	print("Case #"+str((t+1))+": "+str(count))
	outF.write("Case #"+str((t+1))+": "+str(count)+"\n")


outF.close()
inF.close()