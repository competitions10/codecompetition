inF=open("A2_valid.txt","r")
outF=open("A2_out.txt","w")
# 1. F is not important


T=int(inF.readline())
for t in range(T):
	N=int(inF.readline())
	S=inF.readline()[:-1]
	cdict={}
	prevDict={}

	count=0

	for a in range(len(S)-1,0-1,-1):
		for b in range(len(S)-1,a-1,-1):
			if((a+1,b) in cdict):
				value=cdict[(a+1,b)]
				if(S[a]!=prevDict[(a+1,b)] and prevDict[(a+1,b)]!='F' and S[a]!='F'):
					value+=1
				prevDict[(a,b)]=S[a] if (S[a]=='X' or S[a]=='O') else prevDict[(a+1,b)]
				cdict[(a,b)]=value
				count+=value
				# print("Count",a,b,"=",value)
			else:
				cdict[(a,b)]=0
				prevDict[(a,b)]=S[a]
				# print("Count",a,b,"=",0)



	print("Case #"+str((t+1))+": "+str(count))
	outF.write("Case #"+str((t+1))+": "+str(count)+"\n")


outF.close()
inF.close()