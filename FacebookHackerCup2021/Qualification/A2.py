import queue

# inF=open("consistency_chapter_2_input.txt","r")
inF=open("A2_valid.txt","r")
outF=open("A2_out.txt","w")

T=int(inF.readline())


for t in range(T):
	path={}
	pathR={} #reverse path, check if target possible to reach
	S=inF.readline()[:-1]
	K=int(inF.readline())
	for k in range(K):
		kpath=inF.readline()[:-1]
		if(kpath[0] not in path):
			path[kpath[0]]=[]
		path[kpath[0]].append(kpath[1])

		if(kpath[1] not in pathR):
			pathR[kpath[1]]=[]
		pathR[kpath[1]].append(kpath[0])

	print(S)
	# print(path)
	# print(pathR)

	# Repeat A-Z target
	totalCost={}
	letter="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	for a in range(len(letter)):
		cacheDistance={}
		# Iterate target A-Z and see if all match
		isMatch=True
		totalCost[a]=0
		for s in S:
			if(s != letter[a]):
				isMatch=False
				break
		if(isMatch):			
			break

		if(letter[a] in pathR):
			# Iterate every letter to find total cost
			for s in S:
				if(s!=letter[a]):
					if(s in cacheDistance):
						totalCost[a]+=cacheDistance[s]
						# print(s,"=>",letter[a]," distance",distance," total:",totalCost[a])
					else:
						# if need convert, then do bfs and shortest path as well
						bfsIterate=0
						distance=0
						found=False
						flag={}
						bfs=queue.Queue()
						# initial setup
						bfs.put(s)
						flag[s]=True
						bfsIterate=bfs.qsize()

						while(bfs.empty()==False):
							node=bfs.get()
							bfsIterate-=1

							if(node==letter[a]):
								found=True
								break

							#Iterate
							if(node in path):
								for nextNode in path[node]:
									if(nextNode not in flag):
										bfs.put(nextNode)
										flag[nextNode]=True

							if(bfsIterate==0):
								bfsIterate=bfs.qsize()
								distance+=1

						if(found):
							totalCost[a]+=distance
							# print(s,"=>",letter[a]," distance",distance," total:",totalCost[a])
							cacheDistance[letter[a]]=distance
						else:
							totalCost[a]=-1
							break



		else:
			totalCost[a]=-1

	string=""
	for cost in totalCost:
		if(totalCost[cost]!=-1):
			string+=letter[cost]+":"+str(totalCost[cost])+" "
	print(string)

	minCost=-1
	for cost in totalCost:
		if(minCost==-1 or totalCost[cost]>-1 and minCost>-1 and totalCost[cost]<minCost):
			minCost=totalCost[cost]

	print("Case #"+str((t+1))+": "+str(minCost)+"\n")
	outF.write("Case #"+str((t+1))+": "+str(minCost)+"\n")


outF.close()
inF.close()