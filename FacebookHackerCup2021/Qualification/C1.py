import queue

inF=open("gold_mine_chapter_1_input.txt","r")
outF=open("C1_out.txt","w")

T=int(inF.readline())


for t in range(T):
	N = int(inF.readline())
	gold = list(map(int,inF.readline().split(' ')))
	links={}
	for n in range(N-1):
		linkInput=list(map(int,inF.readline().split(' ')))
		if(linkInput[0] not in links):
			links[linkInput[0]]=[]			
		links[linkInput[0]].append(linkInput[1])

		if(linkInput[1] not in links):
			links[linkInput[1]]=[]			
		links[linkInput[1]].append(linkInput[0])

	# find two maximum tree leaves and connect them
	leaves={}
	flag={}
	bfs=queue.Queue()

	bfs.put((1,-1,gold[0]))	# node , root, gold
	flag[1]=True

	while bfs.empty()==False:
		bfsData=bfs.get()
		node=bfsData[0]
		root=bfsData[1]
		g=bfsData[2]

		# print("node", node,root,":", g)
		#iterate
		hasNext=False
		if(node in links):
			for link in links[node]:
				if(link not in flag):
					hasNext=True
					flag[link]=True
					if(node==1):
						nextRoot=link
					else:
						nextRoot=root
					bfs.put((link,nextRoot,g+gold[link-1]))


		if(not hasNext):
			#is leaf
			if(root in leaves):
				if(leaves[root]<g):
					leaves[root]=g
			else:
				leaves[root]=g


	maxLeaf=max(leaves.values())
	
	for key in leaves:
		if(leaves[key]==maxLeaf):
			leaves.pop(key)
			break

	

	maxLeaf2=gold[0]
	if(len(leaves)>0):
		maxLeaf2=max(leaves.values())

	print("max leaf",maxLeaf, "maxLeaf2",maxLeaf2, "gold",gold[0])
	
	ans=maxLeaf+maxLeaf2-gold[0]
	

	print("Case #"+str((t+1))+": "+str(ans)+"\n")
	outF.write("Case #"+str((t+1))+": "+str(ans)+"\n")


outF.close()
inF.close()

545655749