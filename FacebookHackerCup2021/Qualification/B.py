
inF=open("xs_and_os_input.txt","r")
outF=open("B_out.txt","w")

T=int(inF.readline())


for t in range(T):
	N=int(inF.readline())
	board=[]
	#read the board
	for y in range(N):
		board.append(inF.readline()[:-1])

	win={}
	oneDotEliminate={}
	for row in range(N):
		Xcount=0
		dotCount=0
		Ocount=0
		dotCol=-1
		key="r"+str(row)
		for col in range(N):
			pos=board[row][col]
			if(pos=='O'):
				Ocount=1
				break
			elif(pos=='X'):
				Xcount+=1
			elif(pos=='.'):
				dotCount+=1
				dotCol=col

		if(dotCount>0 and Ocount==0):
			win[key]=dotCount
		if(dotCount==1 and Ocount==0):
			oneDotEliminate[str(row)+","+str(dotCol)]=True

	for col in range(N):
		Xcount=0
		dotCount=0
		Ocount=0
		dotRow=-1
		key="c"+str(col)
		for row in range(N):
			pos=board[row][col]
			if(pos=='O'):
				Ocount=1
				break
			elif(pos=='X'):
				Xcount+=1
			elif(pos=='.'):
				dotCount+=1
				dotRow=row

		if(dotCount==1 and Ocount==0 and str(dotRow)+","+str(col) in oneDotEliminate):
			dotCount=1
		elif(dotCount>0 and  Ocount==0):
			win[key]=dotCount

	#iterate all win key 
	combination={}
	minCount=-1
	for key in win:
		count=win[key]
		if(minCount==-1 or count!=-1 and minCount!=-1 and count<minCount):
			minCount=count

		if(count not in combination):
			combination[count]=0
		combination[count]+=1

	ans=""
	if(minCount==-1):
		ans="Impossible"
	else:
		ans=str(minCount)+" "+str(combination[minCount])



	print("Case #"+str((t+1))+": "+ans+"\n")
	outF.write("Case #"+str((t+1))+": "+ans+"\n")


outF.close()
inF.close()