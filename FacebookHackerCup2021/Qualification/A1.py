inF=open("A1_valid.txt","r")
outF=open("A1_out.txt","w")

T=int(inF.readline())

def isVowel(s):
	if(s=='A' or s=='E' or s=='I' or s=='O' or s=='U'):
		return True
	else:
		return False


for t in range(T):
	S=inF.readline()[:-1]
	
	vowelDict={}
	nonVowelDict={}
	vowelTotal=0
	nonVowelTotal=0
	vowelDuplicate='A'
	vowelDuplicateCount=0
	nonVowelDuplicate='B'
	nonVowelDuplicateCount=0
	cases=[]

	# count all letters
	for s in S:
		if(isVowel(s)):
			vowelTotal+=1
			if(s in vowelDict):
				vowelDict[s]+=1
			else:
				vowelDict[s]=1
		else:
			nonVowelTotal+=1
			if(s in nonVowelDict):
				nonVowelDict[s]+=1
			else:
				nonVowelDict[s]=1
	
	# find max duplicate
	for k in vowelDict:
		if(vowelDict[k]>vowelDuplicateCount):
			vowelDuplicate=k
			vowelDuplicateCount=vowelDict[k]

	for k in nonVowelDict:
		if(nonVowelDict[k]>nonVowelDuplicateCount):
			nonVowelDuplicate=k
			nonVowelDuplicateCount=nonVowelDict[k]

	# find min in all case
	print("vowelTotal:"+str(vowelTotal)+" d:"+str(vowelDuplicateCount)+" non:"+str(nonVowelTotal)+" d:"+str(nonVowelDuplicateCount))
	cases.append((vowelTotal-vowelDuplicateCount)*2+nonVowelTotal)
	cases.append((nonVowelTotal-nonVowelDuplicateCount)*2+vowelTotal)


	outF.write("Case #"+str((t+1))+":"+str(min(cases))+"\n")


outF.close()
inF.close()