inF=open("D_second_flight_input.txt","r")
outF=open("D_out_2.txt","w")

T=int(inF.readline())
for t in range(T):
    N,M,Q = map(int,inF.readline().split()) # Airports , Edge, Days
    nodes = {}
    print("#case ",t,":",N,M,Q)
    for n in range(N):
        nodes[n] = {}
        
    for m in range(M):
        A,B,C = map(int,inF.readline().split()) # A, B airport, Capacity
        if(not A in nodes):
            nodes[A]={}
        if(not B in nodes):
            nodes[B]={}

        nodes[A][B] = C
        nodes[B][A] = C
    
    totalList=[]
    cacheResult={}
    for q in range(Q):
        if(q%100 ==0):
            print("day",q)
        total = 0
        From, To = map(int,inF.readline().split()) # From, To
        
        if(not (From,To) in cacheResult):
            for airport,capacity in nodes[From].items():
                if(airport == To):
                    total+=capacity*2

                if(To in nodes[airport]):
                    total+=min(nodes[From][airport],nodes[airport][To])

            cacheResult[(From,To)] = total
            cacheResult[(To,From)] = total
        else:
            total = cacheResult[(From,To)]
            
        totalList.append(total)
    
    ans = "Case #"+str((t+1))+": "+(" ".join(map(str,totalList)))+"\n"
    outF.write(ans)
    print("Case #"+str((t+1)))
    # print(ans)


outF.close()
inF.close()
