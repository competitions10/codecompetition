inF = open("second_hands_input.txt", "r")
outF=open("A_out.txt","w")


T = int(inF.readline()) # testcase 
for t in range(T):
    N,K = map(int,inF.readline().split()) # Items, capacity
    items = map(int,inF.readline().split())
    hash = [ 0 for i in range(100)]
    uniqueCount = 0
    doubleCount = 0
    result = True

    for n in items:
        hash[n-1]+=1

        if(hash[n-1]==1):
            uniqueCount +=1

        if(hash[n-1]==2):
            doubleCount +=1
        # break if more than 3 
        if(hash[n-1]>=3):
            result = False
            break
    
    if(result == True):
        capacityLeft = K - doubleCount
        uniqueItemLeft = uniqueCount - doubleCount
        if(capacityLeft*2 >= uniqueItemLeft):
            result = True
        else:
            result = False


    print("Case #"+str((t+1))+": "+("YES" if result else "NO")+"\n")
    outF.write("Case #"+str((t+1))+": "+("YES" if result else "NO")+"\n")
    




outF.close()
inF.close()
