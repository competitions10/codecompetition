
def decimalToBinary(n):
    # converting decimal to binary
    # and removing the prefix(0b)
    return bin(n)[2:].zfill(8)


inF = open("second_second_meaning_input.txt", "r")
outF = open("C1_out.txt","w")

header0 = []
header1 = []
count=0
# pregenerate 100 
for i in range(100):
    codeContent = decimalToBinary(i)
    # consecative = 0
    # for s in codeContent:
    #     if(s == '0'):
    #         consecative+=1
    #         if(consecative>=3):
    #             break
    #     else:
    #         consecative=0

    # if(consecative<3):
    #     header0.append(codeContent)
    # else:
    #     count+=1
    replaced = ("00"+codeContent).replace('0','.')
    replaced = replaced.replace('1','-')
    header0.append(replaced)
    replaced = ("00"+codeContent).replace('0','-')
    replaced = replaced.replace('1','.')
    header1.append(replaced)


print(header1)

print(header0,len(header0),count)

T = int(inF.readline()) # testcase 
for t in range(T):
    N = int(inF.readline()) # codewords count
    code = inF.readline() # given Code

    outF.write("Case #"+str((t+1))+":"+"\n")

    if(code[0]=='.'):
        for i in range(N-1):
            outF.write(header1[i]+"\n")
    else:
        for i in range(N-1):
            outF.write(header0[i]+"\n")


outF.close()
inF.close()
