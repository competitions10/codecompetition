inF = open("second_friend_input.txt", "r")
outF=open("B1_out.txt","w")


T = int(inF.readline()) # testcase 
for t in range(T):
    R,C = map(int,inF.readline().split()) # Row, Colume
    tree = 0
    result = False

    print(R,C)
    treeMap = []
    for r in range(R):
        columns = [*inF.readline().split()[0]]
        treeMap.append(columns)

    for r in range(R):
        for c in range(C):
            if(treeMap[r][c] == '^'):
                tree+=1

    
    if((R==1 or C==1 ) and tree>=1 ):
        result = False
    else:
        result = True

    print("Case #"+str((t+1))+": "+("Possible" if result else "Impossible")+"\n")
    outF.write("Case #"+str((t+1))+": "+("Possible" if result else "Impossible")+"\n")
    if(result):
        for r in range(R):
            if(tree>0):
                outF.write("^"*C+"\n")
            else:
                outF.write("."*C+"\n")


outF.close()
inF.close()
