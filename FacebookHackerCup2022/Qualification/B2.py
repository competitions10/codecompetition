import queue

inF = open("B2_second_second_friend_validation_input.txt", "r")
outF=open("B2_out.txt","w")

TREE = '^'
ORIGINTREE = 'x'
ROCK = '#'
SPACE = '.'

T = int(inF.readline()) # testcase 
for t in range(T):
    R,C = map(int,inF.readline().split()) # Row, Colume
    tree = 0
    result = False

    print(R,C)
    treeMap = []
    treeCount = [[0 for i in range(C)] for i in range(R)]
    totalTree = 0
    result = False

    for r in range(R):
        columns = [*inF.readline().split()[0]]
        treeMap.append(columns)

    for r in range(R):
        for c in range(C):
            target = treeMap[r][c]
            if(target==TREE or target==SPACE):
                if(r+1<R):
                    treeCount[r+1][c] += 1
                if(r-1>=0):
                    treeCount[r-1][c] += 1
                if(c+1<C):
                    treeCount[r][c+1] += 1
                if(c-1>=0):
                    treeCount[r][c-1] += 1

                if(target==TREE):
                    treeMap[r][c] = ORIGINTREE
                    totalTree+=1
                elif(target==SPACE):
                    treeMap[r][c] = TREE
    
    if(totalTree>0):
        q = queue.Queue()
        hasBrokenOriginTree = False

        def CheckTree(r,c):
            global hasBrokenOriginTree

            if(treeCount[r][c] <=1 and treeMap[r][c]==ORIGINTREE):
                hasBrokenOriginTree=True

            if(treeMap[r][c]==TREE and treeCount[r][c]<=1):
                q.put((r,c))
                treeMap[r][c] = SPACE


        # print("\n".join(map(str,treeCount)))

        for r in range(R):
            for c in range(C):
                target = treeMap[r][c]
                CheckTree(r,c)

        

        while(q.empty()==False):
            nextCheck = q.get()
            r = nextCheck[0]
            c = nextCheck[1]
            treeCount[r][c] = -1
            if(r+1<R):
                treeCount[r+1][c] -= 1
                CheckTree(r+1,c)
            if(r-1>=0):
                treeCount[r-1][c] -= 1
                CheckTree(r-1,c)
            if(c+1<C):
                treeCount[r][c+1] -= 1
                CheckTree(r,c+1)
            if(c-1>=0):
                treeCount[r][c-1] -= 1
                CheckTree(r,c-1)

            # print("\n")
            # print("\n".join(map(str,treeCount)))
            if(hasBrokenOriginTree):
                result = False
                break

        if(q.empty()==True):
            result = True
    else:
        result = True
        # place no tree inside

    ans = "Case #"+str((t+1))+": "+ ("Possible" if result else "Impossible") +"\n"
    outF.write(ans)
    if(result):
        for r in range(R):
            for c in range(C):
                target = treeMap[r][c]
                if(totalTree>0):
                    outF.write(TREE if target==TREE or target==ORIGINTREE else target)
                else:
                    outF.write(SPACE if target==TREE else target)
            outF.write("\n")

outF.close()
inF.close()
