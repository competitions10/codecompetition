C=int(input())

for c in range(C):
    L=int(input())
    l=list(map(int,input().split()))
    t=0
    for i in range(L-1):
        m=min(l[i:L])
        mi=l.index(m)
        # print(mi,i-1,(l[i:mi+1])[::-1])
        l=l[:i]+(l[i:mi+1])[::-1]+l[mi+1:L]
        t+=mi-i+1
    print("Case #"+str(c+1)+": "+str(t))
