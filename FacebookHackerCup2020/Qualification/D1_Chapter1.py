inF=open("D1_running_on_fumes_chapter_1_input.txt","r")
outF=open("D1_Chapter1_output.txt","w")

T=int(inF.readline())

import sys
sys.setrecursionlimit(10000)

for t in range(T):
    temp=inF.readline().split()
    N=int(temp[0])
    M=int(temp[1])
    C=[]
    stateHash={}
    stateList=[]

    for i in range(N):
        C.append(int(inF.readline()))
    
    def Traverse(state):
        minCost=-1
        # print(state)
        #just walk
        for i in range(1,state[1]+1):
            # print(" Traverse Walk : ",state,"->",(state[0]+i,state[1]-i))
            cost=0
            if((state[0]+i,state[1]-i) in stateHash):
                cost=stateHash[(state[0]+i,state[1]-i)]
            else:
                if(state[0]+i==N):
                    if((state in stateHash and minCost<stateHash[state]) or state not in stateHash):
                        stateHash[state]=0
                    return 0
                
                cost=Traverse((state[0]+i,state[1]-i))
            if(cost!=-1 and (cost<minCost or minCost==-1)):
                minCost=cost

        #buy and walk
        if(C[state[0]-1]>0):
            for i in range(1,M+1):
                # print(" Traverse Buy Walk : ",state,"->",(state[0]+i,M-i))
                cost=0
                buyCost=C[state[0]-1]
                if((state[0]+i,M-i) in stateHash):
                    cost=stateHash[(state[0]+i,M-i)]
                else:
                    if(state[0]+i==N):
                        if((state in stateHash and minCost<stateHash[state]) or state not in stateHash):
                            stateHash[state]=buyCost
                        return buyCost
                    
                    cost=Traverse((state[0]+i,M-i))
                if(cost!=-1 and (cost+buyCost<minCost or minCost==-1)):
                    minCost=cost+buyCost
        
        # if((state in stateHash and minCost<stateHash[state]) or state not in stateHash):
        stateHash[state]=minCost
        return minCost

    cost=Traverse((1,M))
    print("Case #"+str(t+1)+":"+str(cost),N)
    outF.write("Case #"+str(t+1)+": "+str(cost)+"\n")
    # stateList.append((1,M))
    # while(len(stateList)>0):
    #     Traverse(stateList.pop())




outF.close()
inF.close()