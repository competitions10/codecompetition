inF=open("travel_restrictions_validation_input.txt","r")
outF=open("travel_output.txt","w")
T=int(inF.readline())



for t in range(T):
    C=int(inF.readline())

    m=[[0 if i==j else -1  for j in range(C)]for i in range(C)]

    I=inF.readline()
    O=inF.readline()

    Fi=2
    Tj=0
    
    def checkPoint(f,t,i,j):
        if(I[t]=='N' or O[f]=='N'):
            m[i][j]=1
        else:
            m[i][j]=0

    flag=True
    while(flag):
        flag=False
        for i in range(C):
            for j in range(C):
                if(i!=j):
                    if(m[i][j]==-1):
                        flag=True
                        if(i-j==1 or j-i==1):
                            # adacent
                            checkPoint(i,j,i,j)
                        else:
                            if(i>j):
                                if(m[i][j+1]==0):
                                    checkPoint(j+1,j,i,j)
                                elif(m[i][j+1]==1):
                                    m[i][j]=1
                            else:
                                if(m[i][j-1]==0):
                                    checkPoint(j-1,j,i,j)
                                elif(m[i][j-1]==1):
                                    m[i][j]=1

    
    s=""
    for i in range(C):
        for j in range(C):
            s+= "N" if m[i][j]==1 else "Y"
        s+="\n"

    outF.write("Case #"+str(t+1)+":\n")
    outF.writelines(s)


outF.close()
inF.close()