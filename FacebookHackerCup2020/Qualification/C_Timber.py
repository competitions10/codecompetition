inF=open("C_timber_input.txt","r")
outF=open("C_timber_output.txt","w")
T=int(inF.readline())

import sys
sys.setrecursionlimit(10000000)






for t in range(T):
    N=int(inF.readline())
    timber=[]
    for n in range(N):
        tree=inF.readline().split(' ')
        timber.append((int(tree[0]),int(tree[1][:-1])))
    
    timber.sort(key=lambda x: x[0])
    # print("sorted")
    maxTimber=0
    dfsTree=[]
    minIntervalHash={}
    maxIntervalHash={}
    intervalHash={}

    def appendInterval(inter):
        if(inter not in intervalHash):
            intervalHash[(inter)]=True
            dfsTree.append(inter)

    for i,x in enumerate(timber):
        appendInterval((x[0]-x[1]+0.1,x[0]))
        appendInterval((x[0],x[0]+x[1]-0.1))
        
    
            
    def TreeIterate(interval):
        global maxTimber
        

        finalInterval=(interval[0],interval[1])

        def CheckMaxIterate(finalInterval,newInterval):
            
            if(newInterval[1]>interval[1]):
                finalInterval=(finalInterval[0],newInterval[1])
            if(newInterval[0]<interval[0]):
                finalInterval=(newInterval[0],finalInterval[1])
            return finalInterval
        # print(interval,length)
        
        # find other possible fall down tree
        # find left
        if(interval[0] in minIntervalHash):
            finalInterval=(minIntervalHash[interval[0]],finalInterval[1]) 
        else:
            leftIndex=-1
            for i,x in enumerate(timber):
                if x[0]>=interval[0]:
                    leftIndex=i-1
                    break

            for i in range(leftIndex,-1,-1):
                #fall left
                if interval[0]-timber[i][0]<=0.1:    
                    inter=(timber[i][0]-timber[i][1]+0.1,interval[1])       
                    # appendInterval(inter)
                    finalInterval=CheckMaxIterate(finalInterval,TreeIterate(inter))
                #fall right
                if timber[i][0]+timber[i][1]<=interval[0]:
                    if(interval[0]-(timber[i][0]+timber[i][1])<=0.1):
                        inter=(timber[i][0],interval[1])
                        # appendInterval(inter)
                        finalInterval=CheckMaxIterate(finalInterval,TreeIterate(inter))
        # find right
        if(interval[1] in maxIntervalHash):
            finalInterval=(finalInterval[0],maxIntervalHash[interval[1]]) 
        else:
            rightIndex=len(timber)
            for i,x in enumerate(timber):
                if x[0]>interval[1]:
                    rightIndex=i
                    break
            
            for i in range(rightIndex,len(timber),1):
                #fall left
                if timber[i][0]-timber[i][1]>=interval[1]:
                    if timber[i][0]-timber[i][1]-interval[1]<=0.1:
                        inter=(interval[0],timber[i][0])
                        # appendInterval(inter)
                        finalInterval=CheckMaxIterate(finalInterval,TreeIterate(inter))
                #fall right
                if timber[i][0]-interval[1]<=0.1:
                    # print("fallRight",(interval[0],timber[i][0]+timber[i][1]-0.1))
                    inter=(interval[0],timber[i][0]+timber[i][1]-0.1)
                    # appendInterval(inter)
                    finalInterval=CheckMaxIterate(finalInterval,TreeIterate(inter))

        #Get the max iterate , hash start point end point, return
        if(interval[0] in minIntervalHash):
            if(minIntervalHash[interval[0]]>finalInterval[0]):
                minIntervalHash[interval[0]]=finalInterval[0]
        else:
            minIntervalHash[interval[0]]=finalInterval[0]

        if(interval[1] in maxIntervalHash):
            if(maxIntervalHash[interval[1]]<finalInterval[1]):
                maxIntervalHash[interval[1]]=finalInterval[1]
        else:
            maxIntervalHash[interval[1]]=finalInterval[1]

        length=round(finalInterval[1]-finalInterval[0])
        if(length>maxTimber):
            maxTimber=length

        return finalInterval

    c=0
    while(len(dfsTree)>0):
        tree=dfsTree.pop()
        TreeIterate(tree)

        # c+=1
        # if(c%100==0):
        #     print(N,len(dfsTree))
        
    print("Case #"+str(t+1)+": "+str(maxTimber))
    outF.write("Case #"+str(t+1)+": "+str(maxTimber)+"\n")



outF.close()
inF.close()


# 8 5
# 3 2
# 2 8
# 1 4
# -4 5

