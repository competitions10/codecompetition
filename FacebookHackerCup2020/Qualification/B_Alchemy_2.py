inF=open("B_alchemy_input.txt","r")
outF=open("B_alchemy_output.txt","w")

T=int(inF.readline())


for t in range(T):
    a=0
    b=0
    N=int(inF.readline())
    S=inF.readline()
    S=S[:-1]
    for i in S:
        if(i=='A'):
            a+=1
    b=N-a

    if(b-a>=2 or a-b>=2):
        result=1
    else:
        result=0
    print("Case #"+str(t+1)+":"+("Y" if result==0 else "N"))
    outF.write("Case #"+str(t+1)+":"+("Y" if result==0 else "N")+"\n")

outF.close()
inF.close()