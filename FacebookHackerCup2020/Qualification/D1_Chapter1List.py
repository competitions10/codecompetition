inF=open("D1_running_on_fumes_chapter_1_input.txt","r")
outF=open("D1_Chapter1_output.txt","w")

T=int(inF.readline())

import sys
sys.setrecursionlimit(10000)

for t in range(T):
    temp=inF.readline().split()
    N=int(temp[0])
    M=int(temp[1])
    C=[0]
    stateHash={}
    currentList=[(0,0) for i in range(M)]

    print("Content",N,M)

    for i in range(N):
        C.append(int(inF.readline()))
    
    def Traverse(current):
        
        newList=[-1 for i in range(len(current))]
        offset=current[0][0]+M
        #from startPoint
        for i in range(M):
            if(current[i][0]<=N and C[current[i][0]]>0):
                cost=C[current[i][0]]+current[i][1]
                # print("cost",current[i][0],cost)
                # to end point
                for j in range(1,M+1):
                    pos=current[i][0]+j
                    if(pos>N):
                        break
                    # print("  pos",pos)
                    if(pos<=N and pos>=offset):
                        # print("     [Check] ",pos,offset,newList[pos-offset])
                        if(newList[pos-offset]==-1 or (newList[pos-offset]!=-1 and cost<newList[pos-offset])):
                            # print("     [setcost] ",cost)
                            newList[pos-offset]=cost
            
        return newList

    #setup initial currentList
    for i in range(2,2+M):
        # print("setup",i)
        currentList[i-2]=(i,0)
    cont=True
    for j in range(M):
        if(currentList[j][0]==N):
            cont=False
            break
    # print("Current :",currentList)

    c=0
    while(cont):
        costList=Traverse(currentList)
        
        for j in range(M):
            currentList[j]=(currentList[j][0]+M,costList[j])
        # print("Current :",currentList)
        for j in range(M):
            if(currentList[j][0]==N):
                cont=False
                break
        
        

        c+=1
        if(c%100==0):
            print(currentList[0][0])

    for i in range(M):
        if(currentList[i][0]>N):
            break
        elif(currentList[i][0]==N):
            pass
        elif(currentList[i][0]<N):
            # print("cost:",C[currentList[i][0]])
            if(C[currentList[i][0]]==0):
                # print(currentList[i])
                currentList[i]=(currentList[i][0],-1)
            else:
                currentList[i]=(currentList[i][0],currentList[i][1]+C[currentList[i][0]])
    # print("Current :",currentList)
    cost=currentList[0][1]
    for i in range(len(currentList)):
        if(cost==-1 or currentList[i][1]<cost and currentList[i][1]!=-1):
            cost=currentList[i][1]

    print("Case #"+str(t+1)+":"+str(cost),"total",N)
    outF.write("Case #"+str(t+1)+": "+str(cost)+"\n")


outF.close()
inF.close()