inF=open("B_alchemy_validation_input.txt","r")
outF=open("B_alchemy_output.txt","w")
T=int(inF.readline())



# 0 success
# 1 explode

class TrieNode(object):
    def __init__(self,char:str,result:int,resultchar:str):
        self.char=char
        self.result=result
        self.resultchar=resultchar
        self.children=[]



def add(root,word:str,result:int,resultchar:str):
    # print("ADD："+word+" , "+str(result))
    node=root
    for char in word:
        found = False
        # print(" nodeChildren"+str.join(',',map(lambda x:x.char,node.children)))
        for child in node.children:
            # print("      search:"+child.char)
            if(child.char==char):
                node=child
                found=True
                break
        if(not found):
            # print("  newNode:"+char)
            newNode=TrieNode(char,-1,'')
            node.children.append(newNode)
            node=newNode


    node.result=result
    node.resultchar=resultchar

def search(root,query:str):
    node=root
    if(not root.children):
        # print("SEARCH FALSE A: "+query)
        return False,-1,''
    for char in query:
        notFound=True
        for child in node.children:
            if(child.char==char):
                notFound=False
                node=child
                break
        if(notFound):
            # print("SEARCH FALSE B: "+query)
            return False,-1,''

    if(node.result!=-1):
        return True,node.result,node.resultchar
    else:
        # print("SEARCH FALSE C: "+query)
        return False,node.result,node.resultchar


startNode=TrieNode('R',-1,'')
stoneDFSList=[]

def MergeStone(S):
    hasDifferent=False
    
    
    if(len(S)==1):
        return 0
    
    found,result,resultchar=search(startNode,S)
    
    if(found):
        return 1
    else:
        add(startNode,S,1,'')
        print("Found:"+str(found)+" result:"+str(result)+" : "+S)
        for i in range(len(S)-2):
            s=S[i:i+3]

            if(s[0]==s[1] and s[1]==s[2]):
                pass
            else:
                hasDifferent=True
                m=s[0]
                if(s[1]==s[2]):
                    m=s[1]
                merged=S[:i]+m+S[i+3:]

                found,result,resultchar=search(startNode,merged)

                if(found):
                    return result
                else:
                    add(startNode,merged,-1,'')
                    stoneDFSList.append(merged)
                # result=MergeStone(S[:i]+m+S[i+3:])
                # if(result==0):
                #     add(startNode,S,0,'')
                #     return result
                # else:
                #     pass


        
        return 1




for t in range(6):
    N=int(inF.readline())
    S=inF.readline()
    S=S[:-1]

    stoneDFSList.append(S)
    while(len(stoneDFSList)>0):

        result=MergeStone(stoneDFSList.pop())
        if(result==0):
            break


    print("Case #"+str(t+1)+":"+("Y" if result==0 else "N"))
    outF.write("Case #"+str(t+1)+":"+("Y" if result==0 else "N")+"\n")

    startNode=TrieNode('R',-1,'')
    stoneDFSList.clear()
# add(startNode,"BBAABBB",1,'')
# add(startNode,"BBABB",0,'')
# add(startNode,"AB",1,'')
# print(search(startNode,"BBABB"))


outF.close()
inF.close()