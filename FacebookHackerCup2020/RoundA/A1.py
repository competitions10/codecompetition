inF=open("A1.txt","r")
outF=open("A1Out.txt","w")

T=int(inF.readline())
mod=1000000007

for t in range(T):
	N,K,W=map(lambda x:int(x),inF.readline().split(' '))
	L=list(map(lambda x:int(x),inF.readline().split(' ')))
	AL,BL,CL,DL=map(lambda x:int(x),inF.readline().split(' '))
	H=list(map(lambda x:int(x),inF.readline().split(' ')))
	AH,BH,CH,DH=map(lambda x:int(x),inF.readline().split(' '))
	pList=[]
	roomC=[]
	totalP=1
	p=0
	def getLH(i):
		if(i<len(L)):
			return L[i],H[i]
		else:
			L.append(((AL*L[i-2]+BL*L[i-1]+CL)%DL)+1)
			H.append(((AH*H[i-2]+BH*H[i-1]+CH)%DH)+1)
			return L[i],H[i]

	def modMul(A,B):
		return ((A%mod)*(B%mod))

	def newRoom(i,oldP):
		L,H=getLH(i)
		p=0

		#pop away and find current roomC
		while(len(roomC)>0 and roomC[0][0]<L):
			roomC.pop(0)
			# print("Poped roomC",roomC)

		if(len(roomC)>0):
			#have room
			oldH=roomC[0][1]
			oldL=roomC[len(roomC)-1][0]
			if(H>oldH):
				p=oldP-(oldL-L)*2-oldH+(H-oldH)+W*2+H
			else:
				p=oldP-H+(L+W-oldL)*2+H
		else:
			#no below room
			p=oldP+W*2+H*2

		#pop away lower than current H
		while(len(roomC)>0 and roomC[len(roomC)-1][1]<H):
			roomC.pop(len(roomC)-1)

		roomC.append((L+W,H))

		# print("new room",i,"[L",L,"H",H,"] oldP",oldP,"p",p,"roomC",roomC)
		pList.append(p)
		return p

	for n in range(N):
		p=newRoom(n,p)
		totalP=modMul(totalP,p)%mod

	print("Case #"+str(t+1)+": "+str(totalP))
	outF.write("Case #"+str(t+1)+": "+str(totalP)+"\n")
    

outF.close()
inF.close()